package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.input.ChaseCamera;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.math.Vector2f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.util.BufferUtils;
import java.util.ArrayList;

class realMap {
    int [] fcon = new int [3];
    int [] fvis = new int [3];
    int [] next = new int [3];
    boolean land;
    int cont;
    int lake;
    ArrayList<Integer> view = new ArrayList<Integer>(5);
    Vector3f cent;
    Vector3f norm;
    int city;          
}

/**
 * test
 * @author Slon
 */
public class Main extends SimpleApplication {

    public static int m = 0;
    public static final int mapSize = 14;
    public static Vector3f [] nodes = new Vector3f[10 * mapSize * mapSize + 2];
    
    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }
    
    public static Vector3f VectorCombine3(Vector3f v1, Vector3f v2, Vector3f v3, float f1, float f2, float f3) {
        Vector3f result = new Vector3f();
        result.x = (f1 * v1.x) + (f2 * v2.x) + (f3 * v3.x);
        result.y = (f1 * v1.y) + (f2 * v2.y) + (f3 * v3.y);
        result.z = (f1 * v1.z) + (f2 * v2.z) + (f3 * v3.z);
        return result;
    }
    
    public static int addNode(Vector3f v) {
        int result = 0;
        boolean b = true;
        if (m != 0) {
            for (int o = 0; o < m; o++) {
                if (nodes[o].equals(v)) {
                    result = o;
                    b = false;
                    break;
                }
            }
        }
        if (b) {
            result = m;
            nodes[m] = v;
            m++;
        }
        return result;
    }

    @Override
    public void simpleInitApp() {
        
        flyCam.setEnabled(false);
        ChaseCamera chaseCam = new ChaseCamera(cam, rootNode, inputManager);
        chaseCam.setMinVerticalRotation((float)-Math.PI);
        
        float angy0 = (float) (Math.PI / 2f - Math.atan(1f / 2f));
        float angy1 = (float) (Math.PI / 2f - Math.atan(-1f / 2f));
        float angz0 = (float) (Math.PI / 5f);
        float angz1 = angz0 * 2;
        float angz2 = angz1 * 2;

        float x0 = 0;
        float x1 = (float) (Math.sin(angy0) * Math.sin(angz2));
        float x2 = (float) (Math.sin(angy0) * Math.sin(angz1));
        float y0 = (float) (Math.cos(angy0));
        float y1 = 1;
        float z0 = 0;
        float z1 = (float) (Math.sin(angy0) * Math.cos(angz1));
        float z2 = (float) (Math.sin(angy1) * Math.cos(angz0));
        float z3 = (float) (Math.sin(angy0));
        
        Vector3f [] v = new Vector3f[12];
        
        v[0] = new Vector3f(x0, y1, z0);
        v[1] = new Vector3f(x0, y0, z3);
        v[2] = new Vector3f(x2, y0, z1);
        v[3] = new Vector3f(x1, y0, -z2);
        v[4] = new Vector3f(-x1, y0, -z2);
        v[5] = new Vector3f(-x2, y0, z1);
        v[6] = new Vector3f(x1, -y0, z2);
        v[7] = new Vector3f(x2, -y0, -z1);
        v[8] = new Vector3f(x0, -y0, -z3);
        v[9] = new Vector3f(-x2, -y0, -z1);
        v[10] = new Vector3f(-x1, -y0, z2);
        v[11] = new Vector3f(x0, -y1, z0);   
        
        Vector3f [][] f = new Vector3f[20][3];
        
        f[0][0] = v[0]; f[0][1] = v[1]; f[0][2] = v[2];
        f[1][0] = v[0]; f[1][1] = v[2]; f[1][2] = v[3];
        f[2][0] = v[0]; f[2][1] = v[3]; f[2][2] = v[4];
        f[3][0] = v[0]; f[3][1] = v[4]; f[3][2] = v[5];
        f[4][0] = v[0]; f[4][1] = v[5]; f[4][2] = v[1];
        f[5][0] = v[1]; f[5][1] = v[6]; f[5][2] = v[2];
        f[6][0] = v[2]; f[6][1] = v[6]; f[6][2] = v[7];
        f[7][0] = v[2]; f[7][1] = v[7]; f[7][2] = v[3];
        f[8][0] = v[3]; f[8][1] = v[7]; f[8][2] = v[8];
        f[9][0] = v[3]; f[9][1] = v[8]; f[9][2] = v[4];
        f[10][0] = v[4]; f[10][1] = v[8]; f[10][2] = v[9];
        f[11][0] = v[4]; f[11][1] = v[9]; f[11][2] = v[5];
        f[12][0] = v[5]; f[12][1] = v[9]; f[12][2] = v[10];
        f[13][0] = v[5]; f[13][1] = v[10]; f[13][2] = v[1];
        f[14][0] = v[1]; f[14][1] = v[10]; f[14][2] = v[6];
        f[15][0] = v[6]; f[15][1] = v[11]; f[15][2] = v[7];
        f[16][0] = v[7]; f[16][1] = v[11]; f[16][2] = v[8];
        f[17][0] = v[8]; f[17][1] = v[11]; f[17][2] = v[9];
        f[18][0] = v[9]; f[18][1] = v[11]; f[18][2] = v[10];
        f[19][0] = v[10]; f[19][1] = v[11]; f[19][2] = v[6];
        
        int [][] f_map = new int[20 * mapSize * mapSize][3];
        Mesh [] mesh = new Mesh[20 * mapSize * mapSize];
        
        int i = 0;
        
        for (int j = 0; j < 20; j++) {
            for (int k = 0; k < mapSize; k++) {
                for (int l = 0; l < mapSize; l++) {
                    if ((k + l) < mapSize) {
                        f_map[i][0] = addNode(VectorCombine3(f[j][0], f[j][1], f[j][2], k, l, mapSize - k - l));
                        f_map[i][1] = addNode(VectorCombine3(f[j][0], f[j][1], f[j][2], k + 1, l, mapSize - 1 - k - l));
                        f_map[i][2] = addNode(VectorCombine3(f[j][0], f[j][1], f[j][2], k, l + 1, mapSize - 1 - k - l));
                        i++;
                        if ((k + l) != mapSize - 1) {
                            f_map[i][0] = addNode(VectorCombine3(f[j][0], f[j][1], f[j][2], k + 1, l + 1, mapSize - 2 - k - l));
                            f_map[i][1] = f_map[i - 1][2];
                            f_map[i][2] = f_map[i - 1][1];
                            i++;
                        }
                    }
                }
            }                    
        }
        
        Vector2f[] texCoord = new Vector2f[3];
        texCoord[0] = new Vector2f(0, 0);
        texCoord[1] = new Vector2f(1, 0);
        texCoord[2] = new Vector2f((1f / 2f), (float) (Math.sqrt(3) / 2f));
        
        Vector3f [][] f_visible = new Vector3f[20 * mapSize * mapSize][3];
        float gap = 0.005f;
        
        for (i = 0; i < (20 * mapSize * mapSize); i++) {
            mesh[i] = new Mesh();
            
            f_visible[i][0] = VectorCombine3(nodes[f_map[i][0]], nodes[f_map[i][1]], nodes[f_map[i][2]], 1 - gap * 2, gap, gap);
            f_visible[i][1] = VectorCombine3(nodes[f_map[i][0]], nodes[f_map[i][1]], nodes[f_map[i][2]], gap, 1 - gap * 2, gap);
            f_visible[i][2] = VectorCombine3(nodes[f_map[i][0]], nodes[f_map[i][1]], nodes[f_map[i][2]], gap, gap, 1 - gap * 2);
            
            //m[i].setMode(Mesh.Mode.Triangles);
            mesh[i].setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(f_visible[i]));
            mesh[i].setBuffer(Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoord));
            mesh[i].setBuffer(Type.Index, 3, BufferUtils.createShortBuffer(new short[] {0, 1, 2}));
            mesh[i].updateBound();

        // *************************************************************************
        // First mesh uses one solid color
        // *************************************************************************

        // Creating a geometry, and apply a single color material to it
            Geometry geom = new Geometry("OurMesh", mesh[i]);
            Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
            mat.setColor("Color", ColorRGBA.Blue);
            geom.setMaterial(mat);

        // Attaching our geometry to the root node.
            rootNode.attachChild(geom);
        }
    }

    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
}
